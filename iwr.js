const damage_types = [
    "bludgeoning",
    "piercing",
    "slashing",
    "bleed",
    "fire",
    "cold",
    "acid",
    "electricity",
    "sonic",
    "positive",
    "negative",
    "good",
    "evil",
    "lawful",
    "chaotic",
    "poison",
    "mental",
];
export function IWR1(damage_groups = [], immunities = [], weaknesses = [], resistances = []) {
    // console.log({ damage_groups, immunities, weaknesses, resistances });
    check_validity(damage_groups);

    let applicable_value = [];
    let applicable_type = [];
    let breakdown = "";

    for (let i = 0; i < damage_groups.length; i++) {
        // immunities
        for (let j = 0; j < immunities.length; j++) {
            if (damage_groups[i].types.includes(immunities[j])) {
                breakdown += `Immune to ${immunities[j]}, so took ${damage_groups[i].value} less damage\n`;
                damage_groups = damage_groups.slice(0, i).concat(damage_groups.slice(i + 1));
            }
        }

        // weaknesses
        applicable_value = [];
        applicable_type = [];
        for (let j = 0; j < weaknesses.length; j++) {
            if (damage_groups[i].types.includes(weaknesses[j].type)) {
                applicable_value.push(weaknesses[j].value);
                applicable_type.push(weaknesses[j].type);
            }
        }
        if (applicable_value[0]) {
            breakdown += `Weak to ${
                applicable_type[applicable_value.indexOf(Math.max(...applicable_value))]
            }, so took ${Math.max(...applicable_value)} more damage\n`;
            damage_groups[i].value += Math.max(...applicable_value);
        }

        // resistances
        applicable_value = [];
        applicable_type = [];
        for (let j = 0; j < resistances.length; j++) {
            if (
                (damage_groups[i].types.includes(resistances[j].type) || resistances[j].type === "all") &&
                (!resistances[j].except ||
                    !damage_groups[i].types.filter((value) => resistances[j].except.includes(value))[0])
            ) {
                applicable_value.push(resistances[j].value);
                applicable_type.push(resistances[j].type);
            }
        }
        // Bound applicable values by the most they could reduce damage.
        for (let j = 0; j < applicable_value.length; j++) {
            if (applicable_value[j] > damage_groups[i].value) {
                applicable_value[j] = damage_groups[i].value;
            }
        }
        if (applicable_value[0]) {
            breakdown += `Resistant to ${
                applicable_type[applicable_value.indexOf(Math.max(...applicable_value))]
            }, so took ${Math.min(Math.max(...applicable_value), damage_groups[i].value)} less damage\n`;
            damage_groups[i].value -= Math.min(Math.max(...applicable_value), damage_groups[i].value);
        }
    }

    let total_damage = 0;
    for (let i = 0; i < damage_groups.length; i++) {
        total_damage += damage_groups[i].value;
    }
    breakdown += total_damage;
    output(breakdown);
}

export function IWR2(damage_groups = [], immunities = [], weaknesses = [], resistances = []) {
    check_validity(damage_groups);

    let applicable_value = [];
    let applicable_type = [];
    let applicable_groups = [];
    let breakdown = "";

    // Immunities
    for (let i = 0; i < damage_groups.length; i++) {
        for (let j = 0; j < immunities.length; j++) {
            if (damage_groups[i].types.includes(immunities[j])) {
                breakdown += `Immune to ${immunities[j]}, so took ${damage_groups[i].value} less damage\n`;
                damage_groups = damage_groups.slice(0, i).concat(damage_groups.slice(i + 1));
            }
        }
    }

    // Weaknesses
    applicable_groups = [];
    for (let i = 0; i < damage_groups.length; i++) {
        applicable_value = [];
        applicable_type = [];
        for (let j = 0; j < weaknesses.length; j++) {
            if (damage_groups[i].types.includes(weaknesses[j].type)) {
                applicable_value.push(weaknesses[j].value);
                applicable_type.push(weaknesses[j].type);
            }
        }

        if (applicable_value[0]) {
            applicable_groups.push({
                types: damage_groups[i].types,
                value: Math.max(...applicable_value),
                weakness_type: applicable_type[applicable_value.indexOf(Math.max(...applicable_value))],
            });
        }
    }
    if (applicable_groups[0]) {
        let highest = highest_group(applicable_groups);
        breakdown += `Weak to ${highest.weakness_type}, so took ${highest.value} more damage\n`;
        damage_groups[group_with_types(damage_groups, highest.types)].value += highest.value;
    }

    // Resistances
    let resisted_groups = {}; // The damage groups that are resisted, categorized by resisted type
    let resisted_group_sums = {}; // The amount of resistance that would apply, categorized by resisted type
    let resistance_all_sum = 0; // The sum of "resistance to all" damage that applies

    // Calculate relevant resistances
    for (let i = 0; i < resistances.length; i++) {
        if (resistances[i].type !== "all") {
            resisted_groups[resistances[i].type] = [];
            resisted_group_sums[resistances[i].type] = 0;

            let remaining_resistance = resistances[i].value;

            for (let j = 0; j < damage_groups.length; j++) {
                if (
                    damage_groups[j].types.includes(resistances[i].type) &&
                    (!resistances[i].except ||
                        !damage_groups[j].types.filter((value) => resistances[i].except.includes(value))[0])
                ) {
                    resisted_groups[resistances[i].type].push(damage_groups[j].types);
                    // increment the resistance's effect, accounting for resistance being "used up" from other damage groups
                    resisted_group_sums[resistances[i].type] += Math.min(damage_groups[j].value, remaining_resistance);
                    remaining_resistance -= Math.min(damage_groups[j].value, remaining_resistance);
                }
            }
        } else {
            // Resistance to all
            for (let j = 0; j < damage_types.length; j++) {
                let remaining_resistance = resistances[i].value;

                for (let k = 0; k < damage_groups.length; k++) {
                    if (
                        damage_groups[k].types.includes(damage_types[j]) &&
                        !damage_groups[k].types.filter((value) => resistances[i].except.includes(value))[0]
                    ) {
                        resistance_all_sum += Math.min(damage_groups[k].value, remaining_resistance);
                        remaining_resistance -= Math.min(damage_groups[k].value, remaining_resistance);
                    }
                }

                if (resistances[i].value > remaining_resistance) {
                    breakdown += `Resistant to all, so took ${resistances[i].value - remaining_resistance} less ${
                        damage_types[j]
                    } damage\n`;
                }
            }
        }
    }

    // Apply the largest resistance if there is one
    let resisted_value = 0;
    let resisted_type;
    for (let i in resisted_group_sums) {
        if (resisted_group_sums[i] > resisted_value) {
            resisted_value = resisted_group_sums[i];
            resisted_type = i;
        }
    }
    if (resisted_value > 0) {
        breakdown += `Resistant to ${resisted_type}, so took ${resisted_value} less damage\n`;
    }

    let total_damage = -1 * (resisted_value + resistance_all_sum);

    for (let i = 0; i < damage_groups.length; i++) {
        total_damage += damage_groups[i].value;
    }
    breakdown += total_damage;
    output(breakdown);
}

function check_validity(damage_groups) {
    for (let i = 0; i < damage_groups.length; i++) {
        if (!damage_groups[i].value || !damage_groups[i].types) {
            breakdown += "Malformed Data";
            breakdown += damage_groups[i];
        }
    }
}

function highest_group(applicable_groups) {
    let max_value = 0;
    let max_group;
    for (let i = 0; i < applicable_groups.length; i++) {
        if (applicable_groups[i].value > max_value) {
            max_value = applicable_groups[i].value;
            max_group = applicable_groups[i];
        }
    }
    return max_group;
}

function group_with_types(damage_groups, types) {
    for (let i = 0; i < damage_groups.length; i++) {
        if (damage_groups[i].types == types) {
            return i;
        }
    }
}

function output(breakdown) {
    game.settings.get("pf2e-iwr", "chat_output")
        ? ChatMessage.create({
              content: breakdown.replace(/(?:\r\n|\r|\n)/g, "<br />") + " Adjusted Damage",
          })
        : console.log(breakdown);
}

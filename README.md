Disclaimer: This module is unaffiliated with the Foundry PF2e developers and incomplete. It is for testing purposes only.

# PF2e IWR

An unofficial proof-of-concept for Immunity/Weakness/Resistance handling
Select a creature, target another, roll damage, and check the console log. You should see a list of the target's IWRs' effects on damage, as well as a final adjusted amount.
Some cases will be incorrect, as the system is still in the early stages of adding in the required data. For example it only works for strike attacks.
If there are cases that are incorrect despite the required information being provided, please create an issue or contact me directly in the community discord. I am Bolt.

### Installation

Manual install only as it's not planned to be listed until it can provide correct results the vast majority of the time.
In Foundry setup, click on the Install Module button and put the following path in the Manifest URL.

`https://gitlab.com/pearcebasmanm/pf2e-iwr/-/raw/main/module.json`

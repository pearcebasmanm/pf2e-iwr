import { IWR1, IWR2 } from "./iwr.js";

Hooks.on("init", () => {
    game.settings.register("pf2e-iwr", "damage_instance_interpretation", {
        name: "Alternate Damage Instance Ruling",
        hint: "Toggle on if you're of the opinion that a damage instance means each damage type/category combination, rather than being an attack's damage as a whole.",
        scope: "world",
        type: Boolean,
        default: false,
        config: true,
    });
    game.settings.register("pf2e-iwr", "chat_output", {
        name: "Output To Chat",
        hint: "(instead of outputting to the console)",
        scope: "world",
        type: Boolean,
        default: false,
        config: true,
    });
});

Hooks.on("createChatMessage", (message) => {
    if (!message.data.flags.pf2e.damageRoll || !message.user.targets) return;
    let active_gms = [];
    for (let [id, user] of game.users.entries()) {
        if (user.active && user.isGM) active_gms.push(id);
    }
    if (game.userId != active_gms[0]) return;

    let damage_groups = format_damage_groups(message.data.flags.pf2e.damageRoll.types);
    let immunities;
    let weaknesses = [];
    let resistances = [];

    // IWR
    for (let it = message.user.targets.values(), val = null; (val = it.next().value); ) {
        const target = val.actor.data.data.traits;

        // immunities
        immunities = target.di.value;

        // weaknesses
        for (let i = 0; i < target.dv.length; i++) {
            if (target.dv[i].value && target.dv[i].type) {
                weaknesses.push({ value: target.dv[i].value, type: target.dv[i].type });
            }
        }

        // resistances
        for (let i = 0; i < target.dr.length; i++) {
            if (target.dr[i].value && target.dr[i].type) {
                resistances.push({ value: target.dr[i].value, type: target.dr[i].type });

                if (target.dr[i].exceptions) {
                    resistances[resistances.length - 1]["except"] = target.dr[i].exceptions;
                }
            }
        }

        break;
    }

    game.settings.get("pf2e-iwr", "damage_instance_interpretation")
        ? IWR1(damage_groups, immunities, weaknesses, resistances)
        : IWR2(damage_groups, immunities, weaknesses, resistances);
});

function format_damage_groups(types) {
    let damage_groups = [];
    for (let i in types) {
        if (typeof types[i] === "object") {
            for (let j in types[i]) {
                damage_groups.push({ value: types[i][j], types: [i, j] });
            }
        } else {
            console.log("Check this out:");
            console.log(i);
            console.log(types[i]);
        }
    }
    return damage_groups;
}
